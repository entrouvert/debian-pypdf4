# PyPDF4

PyPDF4 is a pure-python PDF library capable of
splitting, merging together, cropping, and transforming
the pages of PDF files. It can also add custom
data, viewing options, and passwords to PDF files.
It can retrieve text and metadata from PDFs as well
as merge entire files together.

What happened to PyPDF2?  Nothing; it's still available
at https://github.com/mstamy2/PyPDF2 .  For various reasons
claird will eventually explain, I've simply decided to mark
a new "business model" with a slightly-renamed project name.
While PyPDF4 will continue to be available at no charge, I
have strong plans for better ongoing support to start in August 2018.


Homepage (available soon) 
http://claird.github.io/PyPDF4/

## Examples

Please see the `Sample_Code` folder.

## Documentation

Documentation soon will be available, although probably not at  
https://pythonhosted.org/PyPDF4/


## FAQ
Please see
http://claird.github.io/PyPDF4/FAQ.html
(available in early August)


## Tests
PyPDF4 includes a modest (but growing!) test suite built on the unittest framework. All tests are located in the "Tests" folder.
Tests can be run from the command line by:

```bash
python -m unittest Tests.tests
```
